# Práctica Machine Learning 1101

El objetivo final de la práctica es predecir el precio de las viviendas de AirBnB de la ciudad de Madrid a partir de un dataset que se encuentra [aquí](https://public.opendatasoft.com/explore/dataset/airbnb-listings/download/?format=csv&disjunctive.host_verifications=true&disjunctive.amenities=true&disjunctive.features=true&q=Madrid&timezone=Europe/Berlin&lang=en&use_labels_for_header=true&csv_separator=%3B). Se ha optado por una dataset parcial de 14780 registros. El archivo utilizado se ha adjuntado a este repositorio y se llama *airbnb-listings-extract.csv*.

Lo artefactos que se encuentra en este repositorio son:

- **Práctica-JoséÁngelArrechea.ipynb**: Es el notebook completo donde se desarrolla la solución de la práctica.
- Práctica-JoséÁngelArrechea-sin-distancia-puntos-turisticos.ipynb: es un notebook similar al anterior, pero no incluye una característica adicional relativa a la distancias medias de las viviendas de AirBnB a los puntos de interés turísticos de Madrid. Tampoco incluye las conclusiones finales.
- airbnb-listings-extract.csv: como se ha mencionado anteriormente, es el archivo con los datos que se van a analizar.
- turismo_v1_es.xml: Archivo en formato XML de los puntos de interés turístico de Madrid obtenido de  <a href="https://datos.madrid.es/egob/catalogo/300030-10037182-puntos-interes-turistico.xml" target="_blank">aquí</a>.
- puntos_turisticos_madrid.csv: Archivo en formato csv obtenido tras la conversión de turismo_v1_es.xml que se realiza en el notebook completo, Práctica-JoséÁngelArrechea.ipynb.
- train.csv y test.csv: archivo generados en la ejecución de los notebooks. Son archivos de apoyo en proceso de generación de los modelos.


Se ha seguido el siguiente esquema para el análisis de los datos y la obtención de los modelos.

## 1. Librerías principales, opciones y funciones generales
### 1.1 Librerías principales y opciones
Importación de librerías comunes de Python y asignación de opciones básicas
### 1.2 Funciones generales
Funciones que se va a utilizar varias veces a lo largo del notebook. Se evita la repetición de la declaración de arrays.
## 2. Carga de datos. Eliminación de características innecesarias.
### 2.1 Carga de datos y características básicas
Se lee el archivo de datos que se va a analizar *airbnb-listings-extract.csv* y se asigna a un dataframe
### 2.2 Inspección básica
Inspección básica del dataframe: filas, columnas, tipos, análisis superficial de los datos
### 2.3 Eliminación de características innecesarias
Se eliminan las características/columnas que directamente no aportan nada, que no conozcamos su significado o que inicialmente no sabemos tratar. Este punto muchas veces en los procesos de análisis de los modelos no existe y queda incluido dentro de los siguiente puntos.
## 3 Análisis exploratorio de datos (EDA). Exploraty Data Analysis
Lo primero que se hace en esta sección es dividir el dataframe en train y test. El conjunto de datos de test NO se utilizará en ningún momento a lo largo del análisis, excepto al final del proceso, para validar los modelos que se obtengan.
### 3.1 Eliminación de características no esenciales
En este epígrafe se procede a eliminar determinadas columnas, pero previamente analizando superficialmente su contenido. Algunas de ellas serán eliminadas en un primer estudio, pero más adelante podrían ser recuperadadas y ser analizadas para incorporarlas, si fueran interesantes, a los modelos.
### 3.2 Filtrar filas
Se eliman filas que se consideran superfluas para el estudio
### 3.3 Análisis de valores missing
En este apartado se analizan aquellas columnas que tienen muchos datos vacíos y, por tanto, de las que no se pueden obtener información.
### 3.4 Análisis de variables redundantes
#### 3.4.1 Análisis de características redundantes simples
En este apartado se localizarán y eliminarán aquellas columnas que, si bien aparentemente parecen diferentes, en realidad tienen la misma información, ya que se puede deducir el contenido entre ellas, realizando transformaciones simples. Se localizarán estas columnas con una inspección simple de su contenido.
#### 3.4.2 Análisis de correlación entre variables
Se buscan correlaciones entre variables que en un análisis simple no se detectan.
### 3.5 Análisis de outliers, valores 0 y valores nulos
En este epígrafe se comprueba si hay algún outlier, principalmente, en la variable objetivo. También se controlan los valores nulos, y 0 en las columnas numéricas.
#### 3.5.1 Outliers
Búsqueda y tratamiento de outliers
#### 3.5.2 Valores cero
Búsqueda y tratamiento de valores 0 o equivalente a cero en las variables numéricas
#### 3.5.3 Valores nulos
Búsqueda y tratamiento de valores nulo en las variables numéricas
### 3.6 Variables categóricas
En este apartado vamos a analizar las variables no numéricas
#### 3.6.1 Valores nulo
Búsqueda y tratamiento de valores nulo en las variables de tipo object
#### 3.6.2 Valores vacíos
En este apartado se tratarán valores de cadena que se consideren valor vacíos equivalentes al valor 0 en las variable numéricas
#### 3.6.3 Conversión de variables de tipo cadena a tipo numérico
#####  3.6.3.1 Encoding
En este apartado se estudia si es posible y tiene sentido sustituir literales/categorías por números, esto es, realizar operaciones de codificación (encoding) de variables cuyos valores son listas acotadas en forma texto. Se pueden realizar distintas técnicas de encoding, como MeanEncoding, LabelEnconding, etc.
##### 3.6.3.2 Ponderaciones
En este apartado se analizan principalmente variables que tengan listas de valores, normalmente textos, en su contenido. Se analizan y ponderan cada uno de estos valores y se intenta relacionarlos con respecto a la variable objetivo.
## 4. Generación de características
En esta sección se añaden nuevas columnas/características a partir de las ya existentes o por conocimiento del dominio que se está estudiando.
### 4.1 Conversión de características
Convertir el contenido de una variable en otro valor porque es más conveniente para su estudio.
### 4.2 Combinación de características
En este apartado se combinan dos o más columnas/características para crear una nueva característica.
### 4.3 Creación de nuevas características utilizando datos externos
Añadir nuevas caracterítiscas recurriendo a información externa que podemos combinar con el dataset de datos que se está analizando.
## 5 Creación y validación de modelos
Dependiendo del tipo análisis se aplicarán unos u otros modelos. En nuestro caso de un problema de regresión. Por cada uno de los modelos, seguiremos el siguiente esquea
### 5.1 Modelo elegido
#### 5.2 Creción del modelos
Se crea el modelo utilizando los datos de train
#### 5.1.2 Validación
Se valida el modelo utilizando los datos de test. A los datos de test se le aplicarán las mismas transformaciones que se le han realizado a los datos de train. También se utilizarán las mismas medias, encoders, etc.
#### 5.1.3 Importancia y dependencia
Opcionalmente analizaremos la importancia y dependiencia de cada una de las características
#### 5.1.4 Selección de características
Será conveniente seleccionar las características más importantes con el fin de reducir la dimensionalidad y crear modelos más simples.
#### 5.1.5 Conclusión
Analizamos los datos btenidos
## 6 Conclusión final
Se compararán los resultados de los distintos modelos analizados y se elegirá el o los mejores.







